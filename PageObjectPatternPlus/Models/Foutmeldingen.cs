﻿namespace PageObjectPatternPlus.Models
{
    class Foutmeldingen
    {
        //GebruikerDetailPagina
        public const string GebruikerTitel = "Titel komt niet overeen, gevonden titel: ";
        public const string GebruikerNaam = "Naam komt niet overeen, gevonden naam: ";
        public const string GebruikerEmail = "Email komt niet overeen, gevonden email: ";
        public const string GebruikerKennisgebied = "Kennisgebied komt niet voor in lijst.";
        public const string GebruikerGroep = "Groep komt niet voor in lijst.";
        public const string GebruikerKlant = "Klant komt niet voor in lijst.";
        public const string GebruikerCursus = "Cursus komt niet voor in lijst.";

        //GebruikerOverzichtPagina
        public const string GebruikerNietGevonden = "Gebruiker niet gevonden in het Collegaoverzicht.";

        //KlantDetailPagina
        public const string KlantTitel = "Titel komt niet overeen, gevonden titel: ";
        public const string KlantNaam = "Naam komt niet overeen, gevonden naam: ";
        public const string KlantBeschrijving = "Email komt niet overeen, gevonden email: ";
        public const string KlantKennisgebied = "Kennisgebied komt niet voor in lijst.";
        public const string KlantGebruiker = "Groep komt niet voor in lijst.";

        //KlantOverzichtPagina
        public const string KlantNietGevonden = "Klant niet gevonden in het klantenoverzicht.";

        //HeaderMenu
        public const string MenuItemNietGevonden = "MenuItem niet gevonden.";

        //StartPaginaNaLogin
        public const string BerichtNietGevonden = "Bericht niet gevonden.";
    }
}
