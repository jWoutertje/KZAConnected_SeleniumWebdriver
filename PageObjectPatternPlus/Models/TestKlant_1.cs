﻿using System;
using System.Collections.Generic;

namespace PageObjectPatternPlus.Models
{
    class TestKlant_1
    {
        public const string Naam = "TestKlant_1";
        public const string Beschrijving = "Rerum nulla totam saepe sapiente sit quaerat nobis non sed odit a est sit architecto ut omnis eligendi hic delectus culpa consequatur odio magni id illum.";
        public static readonly DateTime KlantSinds = new DateTime(2015, 1, 1);
        public static readonly List<string> Kennisgebieden = new List<string> { "BDD","Specflow ","C#" };
        public static readonly List<string> KZAers = new List<string> { "Wouter Neve", "Ide Koops", "Reinier Kersbergen" };
    }
}
