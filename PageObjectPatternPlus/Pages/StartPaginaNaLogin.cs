﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;
using PageObjectPatternPlus.Models;
using System;
using System.Collections.Generic;

namespace PageObjectPatternPlus.Pages
{
    public class StartPaginaNaLogin : _Base
    {
        [FindsBy(How = How.Id, Using = "micropost_content")]
        private IWebElement _txtTypBericht = null;

        [FindsBy(How = How.CssSelector, Using = "input.btn-primary[type='submit']")]
        private IWebElement _btnPlaatsBericht = null;

        [FindsBy(How = How.CssSelector, Using = "ol.microposts > li")]
        private IList<IWebElement> _listAlleBerichten = null;

        public StartPaginaNaLogin(IWebDriver webDriver)
            : base(webDriver)
        {
        }

        public StartPaginaNaLogin DoTypBericht(string bericht)
        {
            _txtTypBericht.Clear();
            _txtTypBericht.SendKeys(bericht);
            return this;
        }

        public StartPaginaNaLogin DoPlaatsBericht()
        {
            _btnPlaatsBericht.Click();
            return this;
            //Hier niet nodig
        }

        public StartPaginaNaLogin WithBericht(string gebruiker, string bericht)
        {
            DateTime plaatsingsdatum = DateTime.Now.AddHours(-1);
            string tijd = plaatsingsdatum.ToString("HH:mm");
            string datum = plaatsingsdatum.ToString("dd-MM-yyyy");

            foreach (IWebElement element in _listAlleBerichten)
            {
                if (element.FindElement(By.CssSelector("span.user > a")).Text == gebruiker &&
                    element.FindElement(By.CssSelector("span.content")).Text == bericht &&
                    element.FindElement(By.CssSelector("span.timestamp")).Text.Contains(datum) &&
                    element.FindElement(By.CssSelector("span.timestamp")).Text.Contains(tijd))
                {
                    return this;
                }
            }
            Assert.Fail(Foutmeldingen.BerichtNietGevonden);
            return this;
        }
    }
}