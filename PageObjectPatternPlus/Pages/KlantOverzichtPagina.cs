﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using PageObjectPatternPlus.Models;

namespace PageObjectPatternPlus.Pages
{
    public class KlantOverzichtPagina : _Base
    {
        [FindsBy(How = How.CssSelector, Using = "ul.clients > li > a")]
        private IList<IWebElement> _listAlleKlanten = null;

        public KlantOverzichtPagina(IWebDriver webDriver)
            : base(webDriver)
        {
        }

        public KlantDetailPagina DoKlantDetailPagina(string klant)
        {
            foreach (IWebElement element in _listAlleKlanten)
            {
                if (element.Text.Equals(klant))
                {
                    element.Click();
                    return new KlantDetailPagina(WebDriver);
                }
            }
            Assert.Fail(Foutmeldingen.KlantNietGevonden);
            return new KlantDetailPagina(WebDriver);
        }
    }
}
