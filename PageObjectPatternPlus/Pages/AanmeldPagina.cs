﻿using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;

namespace PageObjectPatternPlus.Pages
{
    public class AanmeldPagina : _Base
    {        
        [FindsBy(How = How.Id, Using = "user_name")]
        private IWebElement _txtNaam = null;

        [FindsBy(How = How.Id, Using = "user_email")]
        private IWebElement _txtEmailAdres = null;

        [FindsBy(How = How.Id, Using = "user_password")]
        private IWebElement _txtWachtwoord = null;

        [FindsBy(How = How.Id, Using = "user_password_confirmation")]
        private IWebElement _txtWachtwoordBevestiging = null;

        [FindsBy(How = How.ClassName, Using = "btn-primary")]
        private IWebElement _btnMaakAccountAan = null;

        public AanmeldPagina(IWebDriver webDriver)
            : base(webDriver)
        {
        }

        public AanmeldPagina DoNaam(string naam)
        {
            _txtNaam.SendKeys(naam);
            return this;
        }
        public AanmeldPagina DoEmailAdres(string email)
        {
            _txtEmailAdres.SendKeys(email);
            return this;
        }

        public AanmeldPagina DoWachtwoord(string wachtwoord)
        {
            _txtWachtwoord.SendKeys(wachtwoord);
            return this;
        }

        public AanmeldPagina DoWachtwoordBevestiging(string wachtwoordbevestiging)
        {
            _txtWachtwoordBevestiging.SendKeys(wachtwoordbevestiging);
            return this;
        }

        public CollegaDetailPagina DoMaakAccountAan()
        {
            _btnMaakAccountAan.Click();
            return new CollegaDetailPagina(WebDriver);
        }
    }
}
