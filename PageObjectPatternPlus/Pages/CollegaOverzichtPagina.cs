﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using PageObjectPatternPlus.Models;

namespace PageObjectPatternPlus.Pages
{
    public class CollegaOverzichtPagina : _Base
    {
        [FindsBy(How = How.CssSelector, Using = "ul.users > li > a")]
        private IList<IWebElement> _listAlleCollegas = null;

        public CollegaOverzichtPagina(IWebDriver webDriver)
            : base(webDriver)
        {
        }

        public CollegaDetailPagina DoCollegaDetailPagina(string collega)
        {
            foreach (IWebElement element in _listAlleCollegas)
            {
                if (element.Text.Equals(collega))
                {
                    element.Click();
                    return new CollegaDetailPagina(WebDriver);
                }
            }
            Assert.Fail(Foutmeldingen.GebruikerNietGevonden);
            return new CollegaDetailPagina(WebDriver);
        }
    }
}
