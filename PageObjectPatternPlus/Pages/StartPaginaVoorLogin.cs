﻿using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;

namespace PageObjectPatternPlus.Pages
{
    public class StartPaginaVoorLogin : _Base
    {
        [FindsBy(How = How.Id, Using = "login")]
        public IWebElement _lnkLogIn = null;

        [FindsBy(How = How.Id, Using = "signup")]
        private IWebElement _lnkMeldJeNuAan = null;

        public StartPaginaVoorLogin(IWebDriver webDriver)
            : base(webDriver)
        {
        }

        public InlogPagina DoLogIn()
        {
            _lnkLogIn.Click();
            return new InlogPagina(WebDriver);
        }

        public AanmeldPagina DoMeldJeNuAan()
        {
            _lnkMeldJeNuAan.Click();
            return new AanmeldPagina(WebDriver);
        }
    }
}