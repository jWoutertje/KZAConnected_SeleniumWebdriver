﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;
using PageObjectPatternPlus.Models;
using System.Collections.Generic;

namespace PageObjectPatternPlus.Pages
{
    public class _HeaderFooter : _Base
    {
        [FindsBy(How = How.Id, Using = "start")]
        private IWebElement _lnkStart = null;

        [FindsBy(How = How.Id, Using = "home")]
        private IWebElement _lnkKZACONNECTED = null;

        [FindsBy(How = How.CssSelector, Using = "ul.dropdown-menu > li > a")]
        private IList<IWebElement> _listAlleNavigeerOpties = null;

        public _HeaderFooter(IWebDriver webDriver)
            : base(webDriver)
        {
        }

        public StartPaginaNaLogin DoHome()
        {
            _lnkKZACONNECTED.Click();
            return new StartPaginaNaLogin(WebDriver);
        }

        public _HeaderFooter DoStart()
        {
            _lnkStart.Click();
            return this;
        }

        public CollegaOverzichtPagina DoCollegaOverzicht()
        {
            foreach (IWebElement navigeeroptie in _listAlleNavigeerOpties)
            {
                if (navigeeroptie.Text.Equals("Collega's"))
                {
                    navigeeroptie.Click();
                    return new CollegaOverzichtPagina(WebDriver);
                }
            }
            Assert.Fail(Foutmeldingen.MenuItemNietGevonden);
            return new CollegaOverzichtPagina(WebDriver);
        }

        public KlantOverzichtPagina DoKlantOverzicht()
        {
            foreach (IWebElement navigeeroptie in _listAlleNavigeerOpties)
            {
                if (navigeeroptie.Text.Equals("Klanten"))
                {
                    navigeeroptie.Click();
                    return new KlantOverzichtPagina(WebDriver);
                }
            }
            Assert.Fail(Foutmeldingen.MenuItemNietGevonden);
            return new KlantOverzichtPagina(WebDriver);
        }
    }
}
