﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using PageObjectPatternPlus.Models;

namespace PageObjectPatternPlus.Pages
{
    public class CollegaDetailPagina : _HeaderFooter
    {  
        [FindsBy(How = How.TagName, Using = "h1")]
        private IWebElement _lblTitel = null;

        [FindsBy(How = How.CssSelector, Using = "label.static-text[for='name']")]
        private IWebElement _lblNaam = null;

        [FindsBy(How = How.CssSelector, Using = "label.static-text[for='email']")]
        private IWebElement _lblEmail = null;

        [FindsBy(How = How.CssSelector, Using = "span.label-primary")]
        private IList<IWebElement> _listAlleBlauweLabeltjes = null;

        [FindsBy(How = How.CssSelector, Using = "table.table-condensed > tbody > tr > td")]
        private IList<IWebElement> _listAlleErvaringCellen = null;

        public CollegaDetailPagina(IWebDriver webDriver)
            : base(webDriver)
        {
        }

        public CollegaDetailPagina WithTitel(string titel)
        {
            Assert.AreEqual(titel, _lblTitel.Text, false, Foutmeldingen.GebruikerTitel + _lblTitel.Text);
            return this;
        }

        public CollegaDetailPagina WithNaam(string naam)
        {
            Assert.AreEqual(naam, _lblNaam.Text, false, Foutmeldingen.GebruikerNaam + _lblNaam.Text);
            return this;
        }

        public CollegaDetailPagina WithEmail(string email)
        {
            Assert.AreEqual(email, _lblEmail.Text, false, Foutmeldingen.GebruikerEmail + _lblEmail.Text);
            return this;
        }

        public CollegaDetailPagina WithGroep(string groep)
        {
            foreach (IWebElement blauwLabeltje in _listAlleBlauweLabeltjes)
            {
                if (blauwLabeltje.Text.Contains(groep))
                {
                    IWebElement blok = blauwLabeltje
                         .FindElement(By.XPath(".."))                           //parent
                         .FindElement(By.XPath(".."))                           //parent
                         .FindElement(By.XPath("preceding-sibling::*[1]"))      //sibling
                         .FindElement(By.TagName("small"));
                    if (blok.Text.Contains("Groepen"))
                        return this;
                }
            }
            Assert.Fail(Foutmeldingen.GebruikerGroep);
            return this;
        }

        public CollegaDetailPagina WithKennisgebied(string kennisgebied)
        {
            foreach (IWebElement blauwLabeltje in _listAlleBlauweLabeltjes)
            {
                if (blauwLabeltje.Text.Contains(kennisgebied))
                {
                    IWebElement blok = blauwLabeltje
                         .FindElement(By.XPath(".."))                           //parent
                         .FindElement(By.XPath(".."))                           //parent
                         .FindElement(By.XPath("preceding-sibling::*[1]"))      //sibling
                         .FindElement(By.TagName("small"));
                    if (blok.Text.Contains("Kennisgebieden"))
                        return this;
                }
            }
            Assert.Fail(Foutmeldingen.GebruikerKennisgebied);
            return this;
        }

        public CollegaDetailPagina WithErvaring(string ervaring)
        {
            foreach (IWebElement cel in _listAlleErvaringCellen)
            {
                if (cel.Text.Equals(ervaring))
                {   
                    return this;
                }
            }
            Assert.Fail(Foutmeldingen.GebruikerKlant);
            return this;
        }

        public CollegaDetailPagina WithCursus(string cursus)
        {
            foreach (IWebElement blauwLabeltje in _listAlleBlauweLabeltjes)
            {
                if (blauwLabeltje.Text.Contains(cursus))
                {
                    IWebElement blok = blauwLabeltje
                         .FindElement(By.XPath(".."))                           //parent
                         .FindElement(By.XPath(".."))                           //parent
                         .FindElement(By.XPath("preceding-sibling::*[1]"))      //sibling
                         .FindElement(By.TagName("small"));
                    if (blok.Text.Contains("Cursussen"))
                        return this;
                }
            }
            Assert.Fail(Foutmeldingen.GebruikerCursus);
            return this;
        }
    }
}
