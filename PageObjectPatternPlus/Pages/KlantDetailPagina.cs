﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using PageObjectPatternPlus.Models;

namespace PageObjectPatternPlus.Pages
{
    public class KlantDetailPagina : _Base
    {
        [FindsBy(How = How.TagName, Using = "h1")]
        private IWebElement _lblTitel = null;

        [FindsBy(How = How.CssSelector, Using = "label.static-text[for='name']")]
        private IWebElement _lblNaam = null;

        [FindsBy(How = How.CssSelector, Using = "label.static-text[for='description']")]
        private IWebElement _lblBeschrijving = null;

        [FindsBy(How = How.CssSelector, Using = "span.label-primary")]
        private IList<IWebElement> _listAlleBlauweLabeltjes = null;

        [FindsBy(How = How.CssSelector, Using = "table.table-condensed > tbody > tr > td")]
        private IList<IWebElement> _listAlleKZAersCellen = null;

        public KlantDetailPagina(IWebDriver webDriver)
            : base(webDriver)
        {
        }

        public KlantDetailPagina WithTitel(string titel)
        {
            Assert.AreEqual(titel, _lblTitel.Text, false, Foutmeldingen.KlantTitel + _lblTitel.Text);
            return this;
        }

        public KlantDetailPagina WithNaam(string naam)
        {
            Assert.AreEqual(naam, _lblNaam.Text, false, Foutmeldingen.KlantNaam + _lblNaam.Text);
            return this;
        }

        public KlantDetailPagina WithBeschrijving(string email)
        {
            Assert.AreEqual(email, _lblBeschrijving.Text, false, Foutmeldingen.KlantBeschrijving + _lblBeschrijving.Text);
            return this;
        }

        public KlantDetailPagina WithKennisgebied(string kennisgebied)
        {
            foreach (IWebElement blauwLabeltje in _listAlleBlauweLabeltjes)
            {
                if (blauwLabeltje.Text.Contains(kennisgebied))
                {
                    IWebElement blok = blauwLabeltje
                         .FindElement(By.XPath(".."))                           //parent
                         .FindElement(By.XPath(".."))                           //parent
                         .FindElement(By.XPath("preceding-sibling::*[1]"))      //sibling
                         .FindElement(By.TagName("small"));
                    if (blok.Text.Contains("Kennisgebieden"))
                        return this;
                }
            }
            Assert.Fail(Foutmeldingen.KlantKennisgebied);
            return this;
        }

        public KlantDetailPagina WithKZAers(string kZAer)
        {
            foreach (IWebElement cel in _listAlleKZAersCellen)
            {
                if (cel.Text.Equals(kZAer))
                {
                    return this;
                }
            }
            Assert.Fail(Foutmeldingen.GebruikerKlant);
            return this;
        }
    }
}
