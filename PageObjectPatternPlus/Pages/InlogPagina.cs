﻿using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;

namespace PageObjectPatternPlus.Pages
{
    public class InlogPagina : _Base
    {
        [FindsBy(How = How.Id, Using = "session_email")]
        private IWebElement _txtEmailAdres = null;

        [FindsBy(How = How.Id, Using = "session_password")]
        private IWebElement _txtWachtwoord = null;

        [FindsBy(How = How.ClassName, Using = "btn-primary")]
        private IWebElement _btnLogIn = null;

        public InlogPagina(IWebDriver webDriver)
            : base(webDriver)
        {
        }

        public InlogPagina DoEmailAdres(string email)
        {
            _txtEmailAdres.SendKeys(email);
            return this;
        }

        public InlogPagina DoWachtwoord(string wachtwoord)
        {
            _txtWachtwoord.SendKeys(wachtwoord);
            return this;
        }

        public CollegaDetailPagina DoLogIn()
        {
            _btnLogIn.Click();
            return new CollegaDetailPagina(WebDriver);
        }
    }
}