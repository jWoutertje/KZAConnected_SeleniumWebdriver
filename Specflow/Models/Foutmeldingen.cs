﻿namespace Specflow.Models
{
    class Foutmeldingen
    {
        //GebruikerDetailPagina
        public const string CollegaTitel = "Titel komt niet overeen, gevonden titel: ";
        public const string CollegaNaam = "Naam komt niet overeen, gevonden naam: ";
        public const string CollegaEmail = "Email komt niet overeen, gevonden email: ";
        public const string CollegaKennisgebied = "Kennisgebied komt niet voor in lijst.";
        public const string CollegaGroep = "Groep komt niet voor in lijst.";
        public const string CollegaKlant = "Klant komt niet voor in lijst.";
        public const string CollegaCursus = "Cursus komt niet voor in lijst.";

        //GebruikerOverzichtPagina
        public const string CollegaNietGevonden = "Collega niet gevonden in het Collegaoverzicht.";

        //KlantDetailPagina
        public const string KlantTitel = "Titel komt niet overeen, gevonden titel: ";
        public const string KlantNaam = "Naam komt niet overeen, gevonden naam: ";
        public const string KlantBeschrijving = "Email komt niet overeen, gevonden email: ";
        public const string KlantKennisgebied = "Kennisgebied komt niet voor in lijst.";
        public const string KlantGebruiker = "Groep komt niet voor in lijst.";

        //KlantOverzichtPagina
        public const string KlantNietGevonden = "Klant niet gevonden in het klantenoverzicht.";

        //HeaderMenu
        public const string MenuItemNietGevonden = "MenuItem niet gevonden.";
    }
}
