﻿	#language: nl-NL
	Functionaliteit: Aanmelden collega

	ALS KZAer
	WIL IK mezelf kunnen aanmelden bij KZA Connected
	ZODAT ik aan collega's kan laten zien waar ik gedetacheerd zit, bij welke groepen ik actief ben,
	welke cursussen ik heb gevolgd en wat mijn kennisgebieden zijn
	EN ik inzicht kan krijgen in soortgelijke informatie van mijn collegas

	Business rules
	- Bij aanmelden zijn de volgende velden verplicht: Naam, Email, Wachtwoord, Wachtwoord bevestiging
	- Het opgegeven emailadres is een valide emailadres
	- Het opgegeven emailadres is uniek
	- Het opgegeven wachtwoord bestaat uit minimaal 6 karakters
	- De wachtwoordbevestiging is identiek aan het opgegeven wachtwoord

	Abstract Scenario: Aanmelden collega
		Gegeven een collega van KZA met naam <Naam> en emailadres <Email>
		Als ik mijzelf aanmeld bij KZA Connected
		Dan wordt mijn aanmelding geaccepteerd en ben ik direct ingelogd
	Voorbeelden: 
		| Naam   | Email         |
		| Wouter | wneve10@kza.nl |  

	#Abstract Scenario: Aanmelden collega zonder naam
	#Abstract Scenario: Aanmelden collega zonder email
	#Abstract Scenario: Aanmelden collega met invalide email
	#Abstract Scenario: Aanmelden collega zonder wachtwoord
	#Abstract Scenario: Aanmelden collega met onjuiste wachtwoordbevestiging
	#Abstract Scenario: Aanmelden collega met reeds geregistreerd emailadres

