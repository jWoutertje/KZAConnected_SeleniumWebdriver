﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using TechTalk.SpecFlow;
using Specflow.Models;

namespace Specflow.Steps
{
    [Binding]
    public sealed class AanmeldenCollegaSteps : _Base
    {
        [Given(@"een collega van KZA met naam (.*) en emailadres (.*)")]
        public void GegevenEenCollegaVanKZAMetNaamEnEmailadres(string naam, string email)
        {
            ScenarioContext.Current.Add("naam", naam);
            ScenarioContext.Current.Add("email", email);
        }

        [When(@"ik mijzelf aanmeld bij KZA Connected")]
        public void AlsIkMijzelfAanmeldBijKZAConnected()
        {
            string naam = ScenarioContext.Current.Get<string>("naam");
            string email = ScenarioContext.Current.Get<string>("email");

            MeldGebruikerAan(naam, email, "Welkom01")
                .WithTitel(ScenarioContext.Current)
                .WebDriver.Quit();
        }

        [Then(@"wordt mijn aanmelding geaccepteerd en ben ik direct ingelogd")]
        public void DanWordtMijnAanmeldingGeaccepteerdEnBenIkDirectIngelogd()
        {
            string verwachteTitel = "Gebruikersprofiel";
            string werkelijkeTitel = ScenarioContext.Current.Get<string>("titel");
            Assert.AreEqual(verwachteTitel, werkelijkeTitel, false, Foutmeldingen.CollegaTitel + werkelijkeTitel); 
        }
    }
}
