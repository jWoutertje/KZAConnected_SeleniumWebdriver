﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using TechTalk.SpecFlow;
using Specflow.Models;

namespace Specflow.Pages
{
    public class CollegaDetailPagina : _Base
    {
        [FindsBy(How = How.TagName, Using = "h1")]
        private IWebElement _lblTitel = null;

        [FindsBy(How = How.CssSelector, Using = "label.static-text[for='name']")]
        private IWebElement _lblNaam = null;

        [FindsBy(How = How.CssSelector, Using = "label.static-text[for='email']")]
        private IWebElement _lblEmail = null;

        [FindsBy(How = How.CssSelector, Using = "span.label-primary")]
        private IList<IWebElement> _listAlleBlauweLabeltjes = null;

        [FindsBy(How = How.CssSelector, Using = "table.table-condensed > tbody > tr > td")]
        private IList<IWebElement> _listAlleErvaringCellen = null;

        public CollegaDetailPagina(IWebDriver webDriver)
            : base(webDriver)
        {
        }

        public CollegaDetailPagina WithTitel(ScenarioContext titel)
        {
            titel.Add("titel", _lblTitel.Text);
            //Assert.AreEqual(titel, _lblTitel.Text, false, Foutmeldingen.CollegaTitel + _lblTitel.Text);
            return this;
        }

        public CollegaDetailPagina WithNaam(string naam)
        {
            Assert.AreEqual(naam, _lblNaam.Text, false, Foutmeldingen.CollegaNaam + _lblNaam.Text);
            return this;
        }

        public CollegaDetailPagina WithEmail(string email)
        {
            Assert.AreEqual(email, _lblEmail.Text, false, Foutmeldingen.CollegaEmail + _lblEmail.Text);
            return this;
        }

        public CollegaDetailPagina WithGroep(string groep)
        {
            foreach (IWebElement blauwLabeltje in _listAlleBlauweLabeltjes)
            {
                if (blauwLabeltje.Text.Contains(groep))
                {
                    IWebElement blok = blauwLabeltje
                         .FindElement(By.XPath(".."))                           //parent
                         .FindElement(By.XPath(".."))                           //parent
                         .FindElement(By.XPath("preceding-sibling::*[1]"))      //sibling
                         .FindElement(By.TagName("small"));
                    if (blok.Text.Contains("Groepen"))
                        return this;
                }
            }
            Assert.Fail(Foutmeldingen.CollegaGroep);
            return this;
        }

        public CollegaDetailPagina WithKennisgebied(string kennisgebied)
        {
            foreach (IWebElement blauwLabeltje in _listAlleBlauweLabeltjes)
            {
                if (blauwLabeltje.Text.Contains(kennisgebied))
                {
                    IWebElement blok = blauwLabeltje
                         .FindElement(By.XPath(".."))                           //parent
                         .FindElement(By.XPath(".."))                           //parent
                         .FindElement(By.XPath("preceding-sibling::*[1]"))      //sibling
                         .FindElement(By.TagName("small"));
                    if (blok.Text.Contains("Kennisgebieden"))
                        return this;
                }
            }
            Assert.Fail(Foutmeldingen.CollegaKennisgebied);
            return this;
        }

        public CollegaDetailPagina WithErvaring(string ervaring)
        {
            foreach (IWebElement cel in _listAlleErvaringCellen)
            {
                if (cel.Text.Equals(ervaring))
                {   
                    return this;
                }
            }
            Assert.Fail(Foutmeldingen.CollegaKlant);
            return this;
        }

        public CollegaDetailPagina WithCursus(string cursus)
        {
            foreach (IWebElement blauwLabeltje in _listAlleBlauweLabeltjes)
            {
                if (blauwLabeltje.Text.Contains(cursus))
                {
                    IWebElement blok = blauwLabeltje
                         .FindElement(By.XPath(".."))                           //parent
                         .FindElement(By.XPath(".."))                           //parent
                         .FindElement(By.XPath("preceding-sibling::*[1]"))      //sibling
                         .FindElement(By.TagName("small"));
                    if (blok.Text.Contains("Cursussen"))
                        return this;
                }
            }
            Assert.Fail(Foutmeldingen.CollegaCursus);
            return this;
        }
    }
}
