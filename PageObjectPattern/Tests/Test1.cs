﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using PageObjectPattern.Models;

namespace PageObjectPattern.Tests
{
    [TestClass]
    public class Test1 : TestHelper
    {
        [TestMethod]
        public void AanmeldenNieuweCollega()
        {
            MeldGebruikerAan("Jantje10", "jantje11@tst.nl", "wachtwoordvanjantje")
                .WebDriver.Quit();
        }

        [TestMethod]
        public void InloggenCollega()
        {
            LogGebruikerIn(TestGebruiker_1.Email, TestGebruiker_1.Wachtwoord)
                .WebDriver.Quit();
        }

        [TestMethod]
        public void InloggenAdminCollega()
        {
            LogGebruikerIn(TestAdminGebruiker_1.Email, TestAdminGebruiker_1.Wachtwoord)
                .WebDriver.Quit();
        }

        [TestMethod]
        public void ValiderenCollegaDetailPagina()
        {
            LogGebruikerIn(TestGebruiker_1.Email, TestGebruiker_1.Wachtwoord)
                .DoStart()
                .DoCollegaOverzicht()
                .DoCollegaDetailPagina(TestGebruiker_1.Naam)
                .WithNaam(TestGebruiker_1.Naam)
                .WithEmail(TestGebruiker_1.Email)
                .WithGroep(TestGebruiker_1.Groepen[0])
                .WithKennisgebied(TestGebruiker_1.Kennisgebieden[0])
                .WithErvaring(TestGebruiker_1.Ervaring[0])
                .WithCursus(TestGebruiker_1.Cursussen[0])
                .WebDriver.Quit();
        }

        [TestMethod]
        public void ValiderenKlantDetailPagina()
        {
            LogGebruikerIn(TestGebruiker_1.Email, TestGebruiker_1.Wachtwoord)
                .DoStart()
                .DoKlantOverzicht()
                .DoKlantDetailPagina(TestKlant_1.Naam)
                .WithNaam(TestKlant_1.Naam)
                .WithBeschrijving(TestKlant_1.Beschrijving)
                .WithKennisgebied(TestKlant_1.Kennisgebieden[0])
                .WithKZAers(TestKlant_1.KZAers[0])
                .WebDriver.Quit();
        }

        [TestMethod]
        public void ValiderenGeplaatstBericht()
        {
            LogGebruikerIn(TestGebruiker_1.Email, TestGebruiker_1.Wachtwoord)
                .DoHome()
                .DoTypBericht(TestGebruiker_1.Bericht)
                .DoPlaatsBericht()
                .WithBericht(TestGebruiker_1.Naam,TestGebruiker_1.Bericht)
                .WebDriver.Quit();
        }
    }
}
