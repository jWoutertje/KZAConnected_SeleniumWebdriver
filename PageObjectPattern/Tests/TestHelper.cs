﻿using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using System;
using PageObjectPattern.Pages;

namespace PageObjectPattern.Tests
{
    public class TestHelper
    {
        public IWebDriver OpstartenWebdriverHomePage()
        {
            IWebDriver driverEen = new ChromeDriver();
            driverEen.Navigate().GoToUrl("https://desolate-cove-31451.herokuapp.com");
            driverEen.Manage().Timeouts().ImplicitlyWait(TimeSpan.FromSeconds(5));
            driverEen.Manage().Window.Maximize();
            return driverEen;
        }

        public CollegaDetailPagina LogGebruikerIn(string email, string wachtwoord)
        {
            var driverEen = OpstartenWebdriverHomePage();
            var pagina = new StartPaginaVoorLogin(driverEen);

            pagina
                .DoLogIn()
                .DoEmailAdres(email)
                .DoWachtwoord(wachtwoord)
                .DoLogIn();
            return new CollegaDetailPagina(driverEen);
        }

        public CollegaDetailPagina MeldGebruikerAan(string naam, string email, string wachtwoord)
        {
            var driverEen = OpstartenWebdriverHomePage();
            var pagina = new StartPaginaVoorLogin(driverEen);

            pagina
                .DoMeldJeNuAan()
                .DoNaam(naam)
                .DoEmailAdres(email)
                .DoWachtwoord(wachtwoord)
                .DoWachtwoordBevestiging(wachtwoord)
                .DoMaakAccountAan();
            return new CollegaDetailPagina(driverEen);
        }
    }
}
