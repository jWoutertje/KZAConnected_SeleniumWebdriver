﻿using System;
using System.Collections.Generic;

namespace PageObjectPattern.Models
{
    class TestGebruiker_1
    {
        public const string Naam = "TestGebruiker_1";
        public const string MV = "M";
        public readonly DateTime Geboortedatum = new DateTime(1987, 1, 1);
        public const string Email = "example-1@railstutorial.org";
        public readonly DateTime InDienstTreding = new DateTime(2015, 1, 1);
        public static readonly List<string> Groepen = new List<string> { "Management team", "TA Gilde" };
        public static readonly List<string> Kennisgebieden = new List<string> { "BDD", "Specflow ", ".Net", "C#" };
        public static readonly List<string> Ervaring = new List<string> { "KPN", "Telfort", "Belastingdienst" };
        public static readonly List<string> Cursussen = new List<string> { "TMAP", "ISTQB", "Topics", "PLAT", "Vaardig vragen" };
        public const string Wachtwoord = "Wachtwoord_1";
        public const string Bericht = "Hatseflats!";
    }
}
