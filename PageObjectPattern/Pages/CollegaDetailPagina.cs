﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using PageObjectPattern.Models;

namespace PageObjectPattern.Pages
{
    public class CollegaDetailPagina : _HeaderFooter
    {
        IWebElement _lblTitel;
        IWebElement _lblNaam;
        IWebElement _lblEmail;
        IList<IWebElement> _listAlleBlauweLabeltjes;
        IList<IWebElement> _listAlleErvaringCellen;

        public CollegaDetailPagina(IWebDriver webDriver)
            : base(webDriver)
        {
            _lblTitel = WebDriver.FindElement(By.TagName("h1"));
            _lblNaam = WebDriver.FindElement(By.CssSelector("label.static-text[for='name']"));
            _lblEmail = WebDriver.FindElement(By.CssSelector("label.static-text[for='email']"));
            _listAlleBlauweLabeltjes = WebDriver.FindElements(By.CssSelector("span.label-primary"));
            _listAlleErvaringCellen = WebDriver.FindElements(By.CssSelector("table.table-condensed > tbody > tr > td"));
        }

        public CollegaDetailPagina WithTitel(string titel)
        {
            Assert.AreEqual(titel, _lblTitel.Text, false, Foutmeldingen.GebruikerTitel + _lblTitel.Text);
            return this;
        }

        public CollegaDetailPagina WithNaam(string naam)
        {
            Assert.AreEqual(naam, _lblNaam.Text, false, Foutmeldingen.GebruikerNaam + _lblNaam.Text);
            return this;
        }

        public CollegaDetailPagina WithEmail(string email)
        {
            Assert.AreEqual(email, _lblEmail.Text, false, Foutmeldingen.GebruikerEmail + _lblEmail.Text);
            return this;
        }

        public CollegaDetailPagina WithGroep(string groep)
        {
            foreach (IWebElement blauwLabeltje in _listAlleBlauweLabeltjes)
            {
                if (blauwLabeltje.Text.Contains(groep))
                {
                    IWebElement blok = blauwLabeltje
                         .FindElement(By.XPath(".."))                           //parent
                         .FindElement(By.XPath(".."))                           //parent
                         .FindElement(By.XPath("preceding-sibling::*[1]"))      //sibling
                         .FindElement(By.TagName("small"));
                    if (blok.Text.Contains("Groepen"))
                        return this;
                }
            }
            Assert.Fail(Foutmeldingen.GebruikerGroep);
            return this;
        }

        public CollegaDetailPagina WithKennisgebied(string kennisgebied)
        {
            foreach (IWebElement blauwLabeltje in _listAlleBlauweLabeltjes)
            {
                if (blauwLabeltje.Text.Contains(kennisgebied))
                {
                    IWebElement blok = blauwLabeltje
                         .FindElement(By.XPath(".."))                           //parent
                         .FindElement(By.XPath(".."))                           //parent
                         .FindElement(By.XPath("preceding-sibling::*[1]"))      //sibling
                         .FindElement(By.TagName("small"));
                    if (blok.Text.Contains("Kennisgebieden"))
                        return this;
                }
            }
            Assert.Fail(Foutmeldingen.GebruikerKennisgebied);
            return this;
        }

        public CollegaDetailPagina WithErvaring(string ervaring)
        {
            foreach (IWebElement cel in _listAlleErvaringCellen)
            {
                if (cel.Text.Equals(ervaring))
                {
                    return this;
                }
            }
            Assert.Fail(Foutmeldingen.GebruikerKlant);
            return this;
        }

        public CollegaDetailPagina WithCursus(string cursus)
        {
            foreach (IWebElement blauwLabeltje in _listAlleBlauweLabeltjes)
            {
                if (blauwLabeltje.Text.Contains(cursus))
                {
                    IWebElement blok = blauwLabeltje
                         .FindElement(By.XPath(".."))                           //parent
                         .FindElement(By.XPath(".."))                           //parent
                         .FindElement(By.XPath("preceding-sibling::*[1]"))      //sibling
                         .FindElement(By.TagName("small"));
                    if (blok.Text.Contains("Cursussen"))
                        return this;
                }
            }
            Assert.Fail(Foutmeldingen.GebruikerCursus);
            return this;
        }
    }
}
