﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using PageObjectPattern.Models;

namespace PageObjectPattern.Pages
{
    public class CollegaOverzichtPagina : _Base
    {
        IList<IWebElement> _listAlleCollegas;

        public CollegaOverzichtPagina(IWebDriver webDriver)
            : base(webDriver)
        {
            _listAlleCollegas = WebDriver.FindElements(By.CssSelector("ul.users > li > a"));
        }

        public CollegaDetailPagina DoCollegaDetailPagina(string collega)
        {
            foreach (IWebElement element in _listAlleCollegas)
            {
                if (element.Text.Equals(collega))
                {
                    element.Click();
                    return new CollegaDetailPagina(WebDriver);
                }
            }
            Assert.Fail(Foutmeldingen.GebruikerNietGevonden);
            return new CollegaDetailPagina(WebDriver);
        }
    }
}
