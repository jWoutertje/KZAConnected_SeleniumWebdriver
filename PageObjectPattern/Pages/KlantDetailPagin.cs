﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using PageObjectPattern.Models;

namespace PageObjectPattern.Pages
{
    public class KlantDetailPagina : _Base
    {
        IWebElement _lblTitel;
        IWebElement _lblNaam;
        IWebElement _lblBeschrijving;
        IList<IWebElement> _listAlleBlauweLabeltjes;
        IList<IWebElement> _listAlleKZAersCellen;

        public KlantDetailPagina(IWebDriver webDriver)
            : base(webDriver)
        {
            _lblTitel = WebDriver.FindElement(By.TagName("h1"));
            _lblNaam = WebDriver.FindElement(By.CssSelector("label.static-text[for='name']"));
            _lblBeschrijving = WebDriver.FindElement(By.CssSelector("label.static-text[for='description']"));
            _listAlleBlauweLabeltjes = WebDriver.FindElements(By.CssSelector("span.label-primary"));
            _listAlleKZAersCellen = WebDriver.FindElements(By.CssSelector("table.table-condensed > tbody > tr > td"));
        }

        public KlantDetailPagina WithTitel(string titel)
        {
            Assert.AreEqual(titel, _lblTitel.Text, false, Foutmeldingen.KlantTitel + _lblTitel.Text);
            return this;
        }

        public KlantDetailPagina WithNaam(string naam)
        {
            Assert.AreEqual(naam, _lblNaam.Text, false, Foutmeldingen.KlantNaam + _lblNaam.Text);
            return this;
        }

        public KlantDetailPagina WithBeschrijving(string email)
        {
            Assert.AreEqual(email, _lblBeschrijving.Text, false, Foutmeldingen.KlantBeschrijving + _lblBeschrijving.Text);
            return this;
        }

        public KlantDetailPagina WithKennisgebied(string kennisgebied)
        {
            foreach (IWebElement blauwLabeltje in _listAlleBlauweLabeltjes)
            {
                if (blauwLabeltje.Text.Contains(kennisgebied))
                {
                    IWebElement blok = blauwLabeltje
                         .FindElement(By.XPath(".."))                           //parent
                         .FindElement(By.XPath(".."))                           //parent
                         .FindElement(By.XPath("preceding-sibling::*[1]"))      //sibling
                         .FindElement(By.TagName("small"));
                    if (blok.Text.Contains("Kennisgebieden"))
                        return this;
                }
            }
            Assert.Fail(Foutmeldingen.KlantKennisgebied);
            return this;
        }

        public KlantDetailPagina WithKZAers(string kZAer)
        {
            foreach (IWebElement cel in _listAlleKZAersCellen)
            {
                if (cel.Text.Equals(kZAer))
                {
                    return this;
                }
            }
            Assert.Fail(Foutmeldingen.GebruikerKlant);
            return this;
        }
    }
}
