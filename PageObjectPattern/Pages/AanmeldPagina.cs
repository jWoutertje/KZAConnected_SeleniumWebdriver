﻿using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;

namespace PageObjectPattern.Pages
{
    public class AanmeldPagina : _Base
    {
        IWebElement _txtNaam;
        IWebElement _txtEmailAdres;
        IWebElement _txtWachtwoord;
        IWebElement _txtWachtwoordBevestiging;
        IWebElement _btnMaakAccountAan;

        public AanmeldPagina(IWebDriver webDriver)
            : base(webDriver)
        {
            _txtNaam = WebDriver.FindElement(By.Id("user_name"));
            _txtEmailAdres = WebDriver.FindElement(By.Id("user_email"));
            _txtWachtwoord = WebDriver.FindElement(By.Id("user_password"));
            _txtWachtwoordBevestiging = WebDriver.FindElement(By.Id("user_password_confirmation"));
            _btnMaakAccountAan = WebDriver.FindElement(By.ClassName("btn-primary"));
        }

        public AanmeldPagina DoNaam(string naam)
        {
            _txtNaam.SendKeys(naam);
            return this;
        }
        public AanmeldPagina DoEmailAdres(string email)
        {
            _txtEmailAdres.SendKeys(email);
            return this;
        }

        public AanmeldPagina DoWachtwoord(string wachtwoord)
        {
            _txtWachtwoord.SendKeys(wachtwoord);
            return this;
        }

        public AanmeldPagina DoWachtwoordBevestiging(string wachtwoordbevestiging)
        {
            _txtWachtwoordBevestiging.SendKeys(wachtwoordbevestiging);
            return this;
        }

        public CollegaDetailPagina DoMaakAccountAan()
        {
            _btnMaakAccountAan.Click();
            return new CollegaDetailPagina(WebDriver);
        }
    }
}
