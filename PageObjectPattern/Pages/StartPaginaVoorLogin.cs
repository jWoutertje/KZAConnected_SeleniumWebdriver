﻿using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;

namespace PageObjectPattern.Pages
{
    public class StartPaginaVoorLogin : _Base
    {
        IWebElement _lnkLogIn;
        IWebElement _lnkMeldJeNuAan;

        public StartPaginaVoorLogin(IWebDriver webDriver)
            : base(webDriver)
        {
            _lnkLogIn = WebDriver.FindElement(By.Id("login"));
            _lnkMeldJeNuAan = WebDriver.FindElement(By.Id("signup"));
        }

        public InlogPagina DoLogIn()
        {
            _lnkLogIn.Click();
            return new InlogPagina(WebDriver);
        }

        public AanmeldPagina DoMeldJeNuAan()
        {
            _lnkMeldJeNuAan.Click();
            return new AanmeldPagina(WebDriver);
        }
    }
}