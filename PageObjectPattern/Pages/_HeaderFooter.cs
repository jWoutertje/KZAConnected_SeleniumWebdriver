﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using PageObjectPattern.Models;

namespace PageObjectPattern.Pages
{
    public class _HeaderFooter : _Base
    {
        IWebElement _lnkStart;
        IWebElement _lnkKZACONNECTED;
        IList<IWebElement> _listAlleNavigeerOpties;

        public _HeaderFooter(IWebDriver webDriver)
            : base(webDriver)
        {
            _lnkStart = WebDriver.FindElement(By.Id("start"));
            _lnkKZACONNECTED = WebDriver.FindElement(By.Id("home"));
            _listAlleNavigeerOpties = WebDriver.FindElements(By.CssSelector("ul.dropdown-menu > li > a"));
        }

        public StartPaginaNaLogin DoHome()
        {
            _lnkKZACONNECTED.Click();
            return new StartPaginaNaLogin(WebDriver);
        }

        public _HeaderFooter DoStart()
        {
            _lnkStart.Click();
            return this;
        }

        public CollegaOverzichtPagina DoCollegaOverzicht()
        {
            foreach (IWebElement navigeeroptie in _listAlleNavigeerOpties)
            {
                if (navigeeroptie.Text.Equals("Collega's"))
                {
                    navigeeroptie.Click();
                    return new CollegaOverzichtPagina(WebDriver);
                }
            }
            Assert.Fail(Foutmeldingen.MenuItemNietGevonden);
            return new CollegaOverzichtPagina(WebDriver);
        }

        public KlantOverzichtPagina DoKlantOverzicht()
        {
            foreach (IWebElement navigeeroptie in _listAlleNavigeerOpties)
            {
                if (navigeeroptie.Text.Equals("Klanten"))
                {
                    navigeeroptie.Click();
                    return new KlantOverzichtPagina(WebDriver);
                }
            }
            Assert.Fail(Foutmeldingen.MenuItemNietGevonden);
            return new KlantOverzichtPagina(WebDriver);
        }
    }
}
