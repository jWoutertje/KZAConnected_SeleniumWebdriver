﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;
using PageObjectPattern.Models;
using System;
using System.Collections.Generic;

namespace PageObjectPattern.Pages
{
    public class StartPaginaNaLogin : _Base
    {
        IWebElement _txtTypBericht;
        IWebElement _btnPlaatsBericht;
        IList<IWebElement> _listAlleBerichten;

        public StartPaginaNaLogin(IWebDriver webDriver)
            : base(webDriver)
        {
            _txtTypBericht = WebDriver.FindElement(By.Id("micropost_content"));
            _btnPlaatsBericht = WebDriver.FindElement(By.CssSelector("input.btn-primary[type='submit']"));
            _listAlleBerichten = WebDriver.FindElements(By.CssSelector("ol.microposts > li"));
        }

        public StartPaginaNaLogin DoTypBericht(string bericht)
        {
            _txtTypBericht.Clear();
            _txtTypBericht.SendKeys(bericht);
            return this;
        }

        public StartPaginaNaLogin DoPlaatsBericht()
        {
            _btnPlaatsBericht.Click();
            return new StartPaginaNaLogin(WebDriver);
            //Om de validatie bij WithBericht succesvol te laten verlopen moet je hier opnieuw de pagina instantieren
            //Om deze reden is gebruik van de pagefactory aan te raden
        }

        public StartPaginaNaLogin WithBericht(string gebruiker, string bericht)
        {
            DateTime plaatsingsdatum = DateTime.Now.AddHours(-1);
            string tijd = plaatsingsdatum.ToString("HH:mm");
            string datum = plaatsingsdatum.ToString("dd-MM-yyyy");

            foreach (IWebElement element in _listAlleBerichten)
            {
                if (element.FindElement(By.CssSelector("span.user > a")).Text == gebruiker && 
                    element.FindElement(By.CssSelector("span.content")).Text == bericht &&
                    element.FindElement(By.CssSelector("span.timestamp")).Text.Contains(datum) &&
                    element.FindElement(By.CssSelector("span.timestamp")).Text.Contains(tijd))
                {
                    return this;
                }
            }
            Assert.Fail(Foutmeldingen.BerichtNietGevonden);
            return this;
        }
    }
}