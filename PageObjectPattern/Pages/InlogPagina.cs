﻿using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;

namespace PageObjectPattern.Pages
{
    public class InlogPagina : _Base
    {
        IWebElement _txtEmailAdres;
        IWebElement _txtWachtwoord;
        IWebElement _btnLogIn;

        public InlogPagina(IWebDriver webDriver)
            : base(webDriver)
        {
            _txtEmailAdres = WebDriver.FindElement(By.Id("session_email"));
            _txtWachtwoord = WebDriver.FindElement(By.Id("session_password"));
            _btnLogIn = WebDriver.FindElement(By.ClassName("btn-primary"));
        }

        public InlogPagina DoEmailAdres(string email)
        {
            _txtEmailAdres.SendKeys(email);
            return this;
        }

        public InlogPagina DoWachtwoord(string wachtwoord)
        {
            _txtWachtwoord.SendKeys(wachtwoord);
            return this;
        }

        public CollegaDetailPagina DoLogIn()
        {
            _btnLogIn.Click();
            return new CollegaDetailPagina(WebDriver);
        }
    }
}