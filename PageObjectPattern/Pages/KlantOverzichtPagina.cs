﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using PageObjectPattern.Models;

namespace PageObjectPattern.Pages
{
    public class KlantOverzichtPagina : _Base
    {
        IList<IWebElement> _listAlleKlanten;

        public KlantOverzichtPagina(IWebDriver webDriver)
            : base(webDriver)
        {
            _listAlleKlanten = WebDriver.FindElements(By.CssSelector("ul.clients > li > a"));
        }

        public KlantDetailPagina DoKlantDetailPagina(string klant)
        {
            foreach (IWebElement element in _listAlleKlanten)
            {
                if (element.Text.Equals(klant))
                {
                    element.Click();
                    return new KlantDetailPagina(WebDriver);
                }
            }
            Assert.Fail(Foutmeldingen.KlantNietGevonden);
            return new KlantDetailPagina(WebDriver);
        }
    }
}
