﻿using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;

namespace PageObjectPattern.Pages
{
    public abstract class _Base
    {
        public IWebDriver WebDriver { get; private set; }

        protected _Base(IWebDriver webDriver)
        {
            WebDriver = webDriver;
        }
    }
}
