﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;

namespace Procedureel
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void TestMethod1()
        {
            // Test waarin een testgebruiker van KZAConnected wordt ingelogd en de Gebruikersprofielpagina wordt gevalideerd op Titel (h1 Tag), 
            // weergegeven Naam en weergegeven Emailadres

            // Instantieren ChromeDriver (1). Navigeren naar startpagina KZA Connected app (2).
            // Zetten generieke driversetting 'Implicit wait' naar max 5s (3). Maximaliseren browserwindow (4).
            IWebDriver driverEen = new ChromeDriver();
            driverEen.Navigate().GoToUrl("https://desolate-cove-31451.herokuapp.com");
            driverEen.Manage().Timeouts().ImplicitlyWait(TimeSpan.FromSeconds(5));
            driverEen.Manage().Window.Maximize();

            //klik op login button
            driverEen.FindElement(By.Id("login")).Click();

            //Zet emailadres in emailveld
            driverEen.FindElement(By.Id("session_email")).SendKeys("example-1@railstutorial.org");

            //Zet wachtwoord in wachtwoordveld
            driverEen.FindElement(By.Id("session_password")).SendKeys("Wachtwoord_1");

            //Klik op login button
            driverEen.FindElement(By.ClassName("btn-primary")).Click();

            //Sla de titel van de pagina op in variabele van type IWebElement
            IWebElement _lblGebruikersProfiel = driverEen.FindElement(By.TagName("h1"));

            //Valideer of de titel overeenkomt met 'Gebruikersprofiel'. Zo niet, geef foutmelding terug
            Assert.AreEqual("Gebruikersprofiel", _lblGebruikersProfiel.Text, false, "De weergegeven pagina na aanmelden is de '" + _lblGebruikersProfiel.Text + "' pagina");

            //Sla de naam van de gebruiker op in variabele van type IWebElement
            IWebElement _lblGebruikersNaam = driverEen.FindElement(By.CssSelector("label.static-text[for='name']"));

            //Valideer of de naam overeenkomt met 'TestGebruiker_1'. Zo niet, geef foutmelding terug
            Assert.AreEqual("TestGebruiker_1", _lblGebruikersNaam.Text, false, "De naam van de weergegeven gebruiker is" + _lblGebruikersNaam.Text + "'");
           
            //Sla het emailadres van de gebruiker op in variabele van type IWebElement
            IWebElement _lblEmailadres = driverEen.FindElement(By.CssSelector("label.static-text[for='email']"));

            //Valideer of het emailadres overeenkomt met 'example-1@railstutorial.org'. Zo niet, geef foutmelding terug
            Assert.AreEqual("example-1@railstutorial.org", _lblEmailadres.Text, false, "Het emailadres van de weergegeven gebruiker is" + _lblEmailadres.Text + "'");
            
            //Afsluiten Chromedriver
            driverEen.Quit();
        }
    }
}
